Rails.application.routes.draw do
  #devise_for :users, controllers: { registrations: "users/registrations" }
  root to: "pages#home"

  get '/auth/:provider/callback' => 'user_sessions#create'
  get '/auth/failure' => 'user_sessions#failure'

  # Custom logout
  match '/logout', :to => 'user_sessions#destroy', via: :all
end
