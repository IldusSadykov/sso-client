CUSTOM_PROVIDER_URL = 'http://localhost:3000'

Rails.application.config.middleware.use OmniAuth::Builder do
    provider :sso, ENV["APP_ID"], ENV["APP_SECRET"]
end
