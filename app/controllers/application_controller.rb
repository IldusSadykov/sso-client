class ApplicationController < ActionController::Base
  include Authorization

  protect_from_forgery with: :exception

  responders :flash
  respond_to :html

  def login_required
    if !current_user
      respond_to do |format|
        format.html  {
          redirect_to '/auth/sso'
        }
        format.json {
          render json: { 'error' => 'Access Denied' }.to_json
        }
      end
    end
  end

  def current_user
    return nil unless session[:user_id]
    @current_user ||= User.find_by(uid: session[:user_id]['uid'])
  end
  helper_method :current_user
end
